package nl.craftsmen.resilience4j.demo.service;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ProductResource {
    private final Logger logger =  LoggerFactory.getLogger(ProductResource.class);

    private final Map<Integer, Product> products = new HashMap<>();

    @GetMapping("/products")
    public Collection<Product> getProducts() {
        return products.values();
    }

    @GetMapping("/product/{id}")
    public Product getProduct(@PathVariable("id") Integer id) {
        logger.info("demo-service called for productId {}", id);
        throw new IllegalStateException("Service is in error");
    }

    public ProductResource() {
        addProduct("Coffee", 2.5);
        addProduct("Tea", 1.75);
        addProduct("Cappuccino", 3.25);
    }

    private void addProduct(String productName, double price) {
        int id = products.size() + 1;
        products.put(id, new Product(id, productName, price));
    }

}
