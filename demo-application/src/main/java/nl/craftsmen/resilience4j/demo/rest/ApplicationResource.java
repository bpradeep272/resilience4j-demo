package nl.craftsmen.resilience4j.demo.rest;

import java.util.Collection;

import nl.craftsmen.resilience4j.demo.client.DemoServiceClient;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class ApplicationResource {
    private final static Logger LOGGER = LoggerFactory.getLogger(ApplicationResource.class);

    private final DemoServiceClient serviceClient;

    public ApplicationResource(DemoServiceClient serviceClient) {
        this.serviceClient = serviceClient;
    }

    @GetMapping("/products")
    public Collection<DemoServiceClient.ProductResponse> action() {
        final Collection<DemoServiceClient.ProductResponse> products = serviceClient.getProducts();
        LOGGER.info("Found products, total {}", products.size());
        return products;
    }

    @GetMapping("/all")
    public DemoServiceClient.ProductResponse getFirstProduct() {
        final DemoServiceClient.ProductResponse firstProduct = getProductFor(1);
        final DemoServiceClient.ProductResponse unUsedSecondProduct = getProductFor(2);
        final DemoServiceClient.ProductResponse unUserThirdProduct = getProductFor(3);
        return firstProduct;
    }

    private DemoServiceClient.ProductResponse getProductFor(int id) {
        return serviceClient.getProduct(id);
    }

}
